#!/bin/bash

usage()
{
    echo 'Usage: ./build.sh [Options]'
    echo
    echo '  Options:'
    echo '  ========'
    echo
    echo '    * -u | --uncompilable: try to compile the uncompilable Rust examples alongside the others.'
    echo '          Alternatively, you can use the following flags to select specific uncompilable examples:'
    echo '      * --own: try to compile the "own" example.'
    echo '      * --bad_func: try to compile the "bad_func" example.'
    echo '      * --no_mut: try to compile the "no_mut" example.'
    echo '      * --no_const: try to compile the "no_const" example.'
    echo '    * -c | --cpp_only: only compile the C++ examples.'
    echo '    * -r | --rust_only: only compile the Rust examples.'
    echo '    * -h | --help: print this usage statement.'
}

cpp_compile()
{
    retDir=$(pwd)
    cd cpp
    if [ ! -d "build" ] ; then
        mkdir build
    fi
    cd build
    cmake ..
    make
    # Add file moving later
    cp $(pwd)/mutable $retDir/executables/cpp
    cp $(pwd)/generics/max $retDir/executables/cpp
    cp $(pwd)/generics/point $retDir/executables/cpp
    cp $(pwd)/obj_orient/shapes $retDir/executables/cpp
    cp $(pwd)/obj_orient/webimage $retDir/executables/cpp
    cp $(pwd)/owner/func $retDir/executables/cpp
    cp $(pwd)/owner/own $retDir/executables/cpp
    cp $(pwd)/owner/slicing $retDir/executables/cpp
    cd $retDir
}

rust_compile()
{
    retDir=$(pwd)
    rt=("$@")
    cd rust
    for t in "${rt[@]}"; do
        cargo build --bin "$t"
    done
    # Add file moving later
    cd target/debug
    cp $(pwd)/copy_func $retDir/executables/rust
    cp $(pwd)/good_func $retDir/executables/rust
    cp $(pwd)/lifex $retDir/executables/rust
    cp $(pwd)/max $retDir/executables/rust
    cp $(pwd)/mut $retDir/executables/rust
    cp $(pwd)/point $retDir/executables/rust
    cp $(pwd)/shapes $retDir/executables/rust
    cp $(pwd)/slicing $retDir/executables/rust
    cp $(pwd)/webimage $retDir/executables/rust
    cd $retDir
}

rt=(
    "good_func"
    "copy_func"
    "slicing"
    "mut"
    "lifex"
    "webimage"
    "shapes"
    "max"
    "point"
)

rustTargets=("${rt[@]}")

allUncomp=false
partUncomp=false
cppCompile=false
rustCompile=false

while [ "$1" != "" ]; do
    case $1 in
        -h | --help ) usage
                      exit 0
                      ;;
        -u | --uncompilable ) if [ "partUncomp" = true ] ; then
                                  rustTargets=("${rt[@]}")
                              fi
                              rustTargets+=("own" "bad_func" "no_mut" "no_const")
                              allUncomp=true
                              partUncomp=false
                              shift
                              ;;
        --own ) if [ "$allUncomp" = false ] ; then
                    rustTargets+=("own")
                    partUncomp=true
                fi
                shift
                ;;
        --bad_func ) if [ "$allUncomp" = false ] ; then
                         rustTargets+=("bad_func")
                         partUncomp=true
                     fi
                     shift
                     ;;
        --no_mut ) if [ "$allUncomp" = false ] ; then
                       rustTargets+=("no_mut")
                       partUncomp=true
                   fi
                   shift
                   ;;
        --no_const ) if [ "$allUncomp" = false ] ; then
                         rustTargets+=("no_const")
                         partUncomp=true
                     fi
                     shift
                     ;;
        -c | --cpp_only ) cppCompile=true
                          shift
                          ;;
        -r | --rust_only ) rustCompile=true
                           shift
                           ;;
        * ) usage
            exit 0
            ;;
    esac
done

if [ "$cppCompile" = false ] && [ "$rustCompile" = false ] ; then
    cppCompile=true
    rustCompile=true
fi

if [ -d "executables" ] ; then
    rm -rf executables
fi

mkdir executables
mkdir executables/cpp
mkdir executables/rust

if [ "$cppCompile" = true ] ; then
    cpp_compile
fi

if [ "$rustCompile" = true ] ; then
    rust_compile "${rustTargets[@]}"
fi
