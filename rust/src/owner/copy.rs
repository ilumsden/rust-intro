fn adder(a: i64, b: i64) -> i64 {
    a + b
}

fn main() {
    let i1 = 4 as i64;
    let i2 = 8 as i64;
    let i3 = adder(i1, i2);
    println!("{} + {} = {}", i1, i2, i3);
}
