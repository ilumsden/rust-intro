add_executable(max max.cpp)
add_executable(point point.cpp)

set_target_properties(max point PROPERTIES CXX_STANDARD 11)
