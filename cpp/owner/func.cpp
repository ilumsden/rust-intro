#include <iostream>
#include <string>

std::string concat(std::string a, std::string b) {
    return a + b;
}

int main() {
    std::string s1 = "hello ";
    std::string s2 = "world";
    std::string s3 = concat(s1, s2);
    std::cout << s1 << " + " << s2 << " = " << s3 << std::endl;
}
