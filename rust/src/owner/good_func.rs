fn concat(a: &String, b: &String) -> String {
    format!("{}{}", a, b)
}

/* Alternate form:
 *
 * fn concat(a: &str, b: &str) -> String {
 *     format!("{}{}", a, b)
 * }
 */

fn main() {
    let s1 = String::from("hello ");
    let s2 = String::from("world");
    let s3 = concat(&s1, &s2);
    println!("{} + {} = {}", s1, s2, s3);
}
