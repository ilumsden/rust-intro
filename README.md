# Intro to Rust

This repo stores the code for an introduction to Rust for programmers who already know C++. All examples (with one exception) are written in both C++ and Rust. However, there are several additional files for Rust to represent cases where incorrect use of the language leads to a failed compilation.

Slides that accompany this repo can be found [**here**](https://docs.google.com/presentation/d/1FTxEWY3uY8slG0fpxrqrEMeIGM0cKwYALna47bZKLYU/edit?usp=sharing). If you're following the slides, look at the speaker notes to know when to run the code in this repo.

## Dependencies
This tutorial only requires the standard Rust toolchain (`cargo`, `rustc`, etc.), CMake (v3.1 or higher), and a C++11 compliant compiler. Here are some guides for installing these dependencies:

* Rust:
  * Recommended Method for all platforms: https://www.rust-lang.org/tools/install
    * If running on a UNIX platform, the link above will give you a `wget` command to install the Rust installer/updater `rustup`.
    * If running on a Windows platform, the link above will have a link to download the `rustup` installer.
  * Alternate Methods for all platforms: https://forge.rust-lang.org/other-installation-methods.html
* CMake:
  * Official Install Page: https://cmake.org/install/
  * UNIX Platforms: `cmake` is almost always pre-installed. If it's not, check your package manager before going to the above link.
* C++11 Compiler:
  * Depends drastically on what compiler you want to use.
  * Common compiler compliant versions:
    * `gcc`: 4.8.1 or higher
    * `clang`: 3.3 or higher

## Compiling and Running

Rust and C++ code must be compiled separately. The following sections will explain how to compile each.

*__Note:__ the C++ code in this repo has only been tested using Linux (Ubuntu 18.04). It is (almost) guaranteed to work on macOS without issue. However, the same might not be true for Windows. All Rust code has been tested on Linux using the command line and on Windows using VSCode.*

### C++

* UNIX: Compilation of the C++ code uses the standard CMake workflow. To start, enter the `cpp` directory. Then, make a new directory called `build`, and enter it. From here, run `cmake ..` to run CMake. Once CMake is finished, the examples can be compiled by running `make name` (where name is the name of the example). If you'd rather just compile all of them, just run `make`.
* Windows: TBA

### Rust

Compilation of the Rust code is the same between UNIX and Windows. The only difference is that commmands below must be used with the terminal in UNIX and with PowerShell in Windows (assuming `cargo` wasn't installed on Windows Subsystem for Linux).

Compilation of the Rust code is performed by Cargo. From the `rust` directory, simply run `cargo build --bin name` to compile a specific example. If you'd rather compile them all, run `cargo build`. Do not be surprised when compilation errors appear. Several of the Rust examples are designed to not compile.

## Final Practice Problem

For the workshop, [Advent of Code 2018, problem 2](https://adventofcode.com/2018/day/2) will be used. The solution code will be added at a later date.
