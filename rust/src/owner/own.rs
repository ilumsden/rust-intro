fn main() {
    let s1 = String::from("hello");
    let s2 = s1;
    println!("s1 is {}", s1);
    println!("s2 is {}", s2);
}
