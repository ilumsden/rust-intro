fn fact_internal(val: i32, next_val: i32, inter: &mut i32, builders: &mut Vec<i32>) -> i32 {
    if next_val > val {
        return *inter;
    };
    builders.push(next_val);
    *inter *= next_val;
    fact_internal(val, next_val+1, inter, builders)
}

fn fact(val: i32, builders: &mut Vec<i32>) -> i32 {
    if val == 0 {
        return 1;
    };
    fact_internal(val, 1, &mut 1, builders)
}

fn main() {
    let mut v: Vec<i32> = Vec::new();
    let val = 5;
    let val = fact(val, &mut v);
    print!("5! = {}", v[0]);
    for i in &v[1..] {
        print!(" * {}", i);
    };
    println!(" = {}", val);
}
