fn print_substring(s: &String, low: usize, high: usize) {
    println!("{} from indices {} to {} = {}", s, low, high, &s[low..high]);
}

fn print_subvec(v: &Vec<i32>, low: usize, high: usize) {
    println!("Vector from indices {} to {}:", low, high);
    for val in &v[low..high] {
        println!("  {}", val);
    }
}

fn main() {
    let s = String::from("Hello World");
    let v = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    print_substring(&s, 2, 8);
    print_subvec(&v, 2, 8);
}
