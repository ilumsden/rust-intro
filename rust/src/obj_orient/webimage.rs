struct WebImage {
    width: u64,
    height: u64,
    uri: String,
}

/* Alternate form with Lifetimes:
 *
 * struct WebImage<'a> {
 *     width: u64,
 *     height: u64,
 *     uri: &'a str,
 * }
 */

impl WebImage {
    fn new(w: u64, h: u64, link: String) -> WebImage {
        WebImage { width: w, height: h, uri: link }
    }

    pub fn from(w: u64, h: u64, link: &str) -> WebImage {
        WebImage::new(w, h, String::from(link))
    }

    pub fn get_width(&self) -> u64 {
        self.width
    }

    pub fn set_width(&mut self, w: u64) {
        self.width = w;
    }

    pub fn get_height(&self) -> u64 {
        self.height
    }

    pub fn set_height(&mut self, h: u64) {
        self.height = h;
    }

    pub fn get_uri(&self) -> &str {
        &self.uri
    }

    pub fn set_uri(&mut self, u: &str) {
        self.uri = String::from(u);
    }
}

fn main() {
    let img = WebImage::from(500, 500, "file://localhost/imgs/pic.png");
    println!("Width = {}", img.get_width());
    println!("Height = {}", img.get_height());
    println!("URI = {}", img.get_uri());
}
