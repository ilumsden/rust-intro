#include <iostream>
#include <vector>

// C++ currently does NOT have an equivalent to Rust type bounds.
// C++20 will add "concepts" which will be similar.
template <typename T>
T get_max(std::vector<T>& data)
{
    T maxval = data.at(0);
    for (unsigned int i = 1; i < data.size(); i++)
    {
        if (data.at(i) > maxval)
        {
            maxval = data.at(i);
        }
    }
    return maxval;
}

int main()
{
    std::vector<int> data = {3, 2, 5, 1, 7, 4, 9, 6, 8};
    // The template parameter (<int>) is not needed if compiling with
    // C++17 or higher.
    int max = get_max<int>(data);
    std::cout << "Max value = " << max << std::endl;
}
