fn fact_internal(val: const i32, next_val: i32, inter: i32, builders: &Vec<i32>) -> i32 {
    if next_val > val {
        return inter;
    };
    builders.push(next_val);
    fact_internal(val, next_val+1, inter*next_val, builders)
}

fn fact(val: const i32, builders: &Vec<i32>) -> i32 {
    if val == 0 {
        return 1;
    };
    fact_internal(val, 1, 1, builders)
}

fn main() {
    let v = Vec::new();
    let val = 5;
    val = fact(val, &v);
    print!("5! = {}", v[0]);
    for i in &v[1..] {
        print!(" * {}", i);
    };
    println!(" = {}", val);
}
