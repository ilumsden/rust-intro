#include <iostream>
#include <string>
#include <vector>

void print_substring(std::string s, std::size_t low, std::size_t high)
{
    std::string sub = s.substr(low, (high-low));
    std::cout << s << " from indices " << low << " to " << high << " = " << sub << std::endl;
}

void print_subvec(std::vector<int> v, std::size_t low, std::size_t high)
{
    std::vector<int> subv;
#ifdef VEC
    subv = std::vector<int>(v.begin() + low, v.begin() + high);
#else
    for (std::size_t i = low; i < high; i++)
    {
        subv.push_back(v.at(i));
    }
#endif
    std::cout << "Vector from indices " << low << " to " << high << ":" << std::endl;
    for (int i : subv)
    {
        std::cout << "  " << i << std::endl;
    }
}

int main()
{
#ifdef VEC
    std::cout << "Using VEC macro for iterator construction of subvector\n\n";
#endif
    std::string s = "Hello World";
    std::vector<int> v = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    print_substring(s, 2, 8);
    print_subvec(v, 2, 8);
}
