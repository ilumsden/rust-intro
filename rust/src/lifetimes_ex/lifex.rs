// This is an object which owns a borrow to something,
// rather like our `Multiplier` from before.
// You can ignore the strange <'x> notation for now -
// don't worry, I'll cover it later.
struct RefObject<'x>(&'x u32);

fn steal_a_var<'x>(o: RefObject<'x>) {
    println!("{}", o.0);
}

fn main() {
    // a is created in main()s scope
    let a = 3;
    
    // b is created in main()s scope
    let mut b = &a;
    
    // c is created in main()s scope
    let c = RefObject(&b);
    
    // c is moved out of main()s scope.
    // c now lives as long as steal_a_var()s scope.
    steal_a_var(c);
    
    // steal_a_var()s scope ends, killing all the variables inside it...
    // c goes away
    
    // d is created in main()s scope
    let d = &mut b;
    
}
// main()s scope ends, killing all the variables inside it...
// d goes away, as it was declared last
// b goes away, as it was declared second-last
// a goes away, as it was declared third-last

// Alternate Theoretical View of main
//
// fn main() {
//    // The `'a: {}` syntax used here isn't actually valid, I'm just using it
//    // to show you the scopes in this program.
//    'a: {
//        // a is created, it gets its own scope 'a -
//        // lasting as long as the scope containing it (main()'s scope)
//        let a = 3;
//        'b: {
//            // b is created, it gets its own scope 'b -
//            // lasting as long as the scope containing it
//            let mut b = &a;
//            'c: {
//                // c is created, it gets its own scope 'c -
//                // only lasting until steal_a_var(), as it is moved out
//                let c = RefObject(&b);
//                steal_a_var(c);
//            } // c goes away
//            'd: {
//                // d is created, it gets its own scope 'd -
//                // lasting as long as the scope containing it
//                let d = &mut b;
//            } // d goes away
//        } // b goes away
//    } // a goes away
//}
