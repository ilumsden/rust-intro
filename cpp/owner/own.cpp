#include <iostream>
#include <string>

int main()
{
    std::string s1 = "hello";
    std::string s2 = s1;
    std::cout << "s1 is " << s1 << std::endl;
    std::cout << "s2 is " << s2 << std::endl;
}
