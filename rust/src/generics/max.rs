fn get_max<'a, T: PartialOrd>(data: &'a [T]) -> &'a T {
    let mut max: &T = &data[0];
    for elem in &data[1..] {
        if *elem > *max {
            max = elem;
        };
    };
    max
}

fn main() {
    let data = vec![3, 2, 5, 1, 7, 4, 9, 6, 8];
    // Could also call as get_max::<i32>(&data);
    let max = get_max(&data);
    println!("Max value = {}", *max);
}
