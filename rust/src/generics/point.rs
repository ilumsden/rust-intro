// Could get rid of the trait bound and uncomment the following line
// #[derive(Default)]
struct Point<T: Default> {
    x: T,
    y: T,
    z: T,
}

impl<T: Default> Point<T> {
    fn new() -> Point<T> {
        Point { x: Default::default(), y: Default::default(), z: Default::default() }
    }

    fn make(a: T, b: T, c: T) -> Point<T> {
        Point { x: a, y: b, z: c }
    }

    fn get_x(&self) -> &T {
        &self.x
    }

    fn get_y(&self) -> &T {
        &self.y
    }

    fn get_z(&self) -> &T {
        &self.z
    }
}

fn main() {
    let pt: Point<i32> = Point::new();
    println!("x = {}, y = {}, z = {}", pt.get_x(), pt.get_y(), pt.get_z());
    let pt: Point<f32> = Point::make(3.14, 2.71, 5.0);
    println!("x = {}, y = {}, z = {}", pt.get_x(), pt.get_y(), pt.get_z());
}
