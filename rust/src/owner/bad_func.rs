fn concat(a: String, b: String) -> String {
    // This is an expression.
    // Every line in Rust (that's not continued on subsequent lines) falls
    // into one of two categories:
    //   1) Expression: a line of code that returns a value. Does NOT end
    //                  with a semicolon
    //   2) Statement: a line of code that does not return a value. Ends with
    //                 a semicolon
    format!("{}{}", a, b)
}

fn main() {
    let s1 = String::from("hello ");
    let s2 = String::from("world");
    let s3 = concat(s1, s2);
    println!("{} + {} = {}", s1, s2, s3);
}
