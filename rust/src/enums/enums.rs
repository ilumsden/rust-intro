enum WebEvent {
    PageLoad,
    KeyPress(char),
    TxtCopy(String),
    TxtPaste(String),
    Click { x: u64, y: u64 },
}

fn inspect_event(e: WebEvent) {
    match e {
        WebEvent::PageLoad => println!("Page Loaded"),
        WebEvent::KeyPress(c) => println!("Key \"{}\" pressed", c),
        WebEvent::TxtCopy(s) => println!("Copied \"{}\"", s),
        WebEvent::TxtPaste(s) => println!("Pasted \"{}\"", s),
        WebEvent::Click {x, y} => println!("Clicked at point ({}, {})", x, y),
    }
}

fn main() {
    let pressed = WebEvent::KeyPress('k');
    let pasted = WebEvent::TxtPaste(String::from("Hello World"));
    let copied = WebEvent::TxtCopy(String::from("This is a string."));
    let loaded = WebEvent::PageLoad;
    let click = WebEvent::Click { x: 250, y: 150 };

    inspect_event(pressed);
    inspect_event(pasted);
    inspect_event(copied);
    inspect_event(loaded);
    inspect_event(click);
}
