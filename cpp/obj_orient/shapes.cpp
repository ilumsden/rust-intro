#include <iostream>
#include <memory>
#include <string>
#include <vector>

class Shape
{
    public:
        // No real way to replicate the new_shape function

        virtual float get_area() = 0;
};

class Rectangle : public Shape
{
    public:

        Rectangle(float w, float h)
            : width(w)
            , height(h)
        {  }

        float get_area() override
        {
            return width * height;
        }

        std::vector<float> get_side_lens()
        {
            std::vector<float> lens = { width, height };
            return lens;
        }

    protected:

        float width, height;
};

class Square : public Rectangle
{
    public:

        Square(float len)
            : Rectangle(len, len)
        {  }

        float get_side()
        {
            return width;
        }
};

int main()
{
    Rectangle* rect = new Rectangle(3.0, 5.0);
    std::cout << "rect's area = " << rect->get_area() << std::endl;
    std::vector<float> dims = rect->get_side_lens();
    std::cout << "rec's dimensions: width = " << dims[0] << " height = " << dims[1] << std::endl;
    Square* sq = new Square(3.0);
    std::cout << "sq's area = " << sq->get_area() << std::endl;
    std::cout << "sq's side length: " << sq->get_side() << std::endl;

    // Upcasting: Equivalent of Rust Trait Objects
    Shape* b = sq;
    std::cout << "b's area (as upcasted Shape) = " << b->get_area() << std::endl;
    // The following does NOT work.
    // std::cout << "b's side length (as upcasted Shape): " << b->get_side() << std::endl;

    // Unlike Rust, downcasting works.
    // However, the programmer MUST make sure the downcasting is being done to the actual type.
    // The compiler has almost no way of checking this, and it's extremely unsafe to downcast to
    // a different type than the memory started as.
    // Square* new_sq_ptr = static_cast<Square*>(b);
    // std::cout << "new_sq_ptr's side length: " << new_sq_ptr->get_side() << std::endl;

    // If you wanted to be safe, you could use dynamic_cast and check if the output is nullptr.
    Rectangle* dynamic_sq_ptr = dynamic_cast<Rectangle*>(b);
    if (dynamic_sq_ptr == nullptr)
    {
        std::cout << "Could not cast Shape to Rectangle\n";
    }
    else
    {
        std::vector<float> new_dims = dynamic_sq_ptr->get_side_lens();
        std::cout << "New Rectangle's dimensions: width = " << new_dims[0] << " height = " << new_dims[1] << std::endl;
    }
}
