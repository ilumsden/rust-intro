#include <iostream>

template <typename T>
class Point
{
    public:

        Point() = default;

        Point(T a, T b, T c)
            : x(a)
            , y(b)
            , z(c)
        {  }

        T get_x() { return x; }

        T get_y() { return y; }

        T get_z() { return z; }

    private:

        T x, y, z;
};

int main()
{
    Point<int> pt;
    std::cout << "x = " << pt.get_x() << ", y = " << pt.get_y() << ", z = " << pt.get_z() << std::endl;
    Point<float> pt1(3.14, 2.71, 5.);
    std::cout << "x = " << pt1.get_x() << ", y = " << pt1.get_y() << ", z = " << pt1.get_z() << std::endl;
}
