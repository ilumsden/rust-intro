#include <iostream>
#include <string>

typedef unsigned int uint;

class WebImage {
    public:

        WebImage(uint w, uint h, std::string u)
            : width(w)
            , height(h)
            , uri(u)
        {  }

        uint get_width();

        uint get_height();

        std::string get_uri();

        void set_width(uint w);

        void set_height(uint h);

        void set_uri(std::string s);

    private:

        uint width, height;
        std::string uri;
};

uint WebImage::get_width() 
{
    return width;
}

uint WebImage::get_height()
{
    return height;
}

std::string WebImage::get_uri()
{
    return uri;
}

void WebImage::set_width(uint w)
{
    width = w;
}

void WebImage::set_height(uint h)
{
    height = h;
}

void WebImage::set_uri(std::string s)
{
    uri = s;
}

int main()
{
    WebImage img(500, 500, "file://localhost/imgs/pic.png");
    std::cout << "Width = " << img.get_width() << std::endl;
    std::cout << "Height = " << img.get_height() << std::endl;
    std::cout << "URI = " << img.get_uri() << std::endl;
}
