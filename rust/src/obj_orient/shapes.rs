trait Shape {
    fn new_shape(dims: &[f32]) -> Option<Self>
        where Self: Sized;
    fn get_area(&self) -> f32;
}

struct Rectangle {
    width: f32,
    height: f32,
}

impl Rectangle {
    fn new(w: f32, h: f32) -> Rectangle {
        Rectangle { width: w, height: h }
    }

    fn get_side_lens(&self) -> (f32, f32) {
        (self.width, self.height)
    }
}

impl Shape for Rectangle {
    fn new_shape(dims: &[f32]) -> Option<Rectangle> {
        if dims.len() < 2 {
            return None;
        };
        Some(Rectangle::new(dims[0], dims[1]))
    }

    fn get_area(&self) -> f32 {
        self.width * self.height
    }
}

struct Square {
    rect: Rectangle,
}

impl Square {
    fn new(side_len: f32) -> Square {
        Square { rect: Rectangle::new(side_len, side_len) }
    }

    fn get_side(&self) -> f32 {
        self.rect.width
    }
}

impl Shape for Square {
    fn new_shape(dims: &[f32]) -> Option<Square> {
        if dims.len() == 0 {
            return None;
        }
        Some(Square::new(dims[0]))
    }

    fn get_area(&self) -> f32 {
        self.rect.get_area()
    }
}

fn main() {
    let rect = Rectangle::new(3.0, 5.0);
    println!("rect's area = {}", rect.get_area());
    let (w, h) = rect.get_side_lens();
    println!("rect's dimensions: width = {} height = {}", w, h);
    let sq = Square::new(3.0);
    println!("sq's area = {}", sq.get_area());
    println!("sq's side length: {}", sq.get_side());

    // Trait objects
    let b: Box<dyn Shape> = Box::new(sq);
    println!("b's area (as Trait Obj) = {}", b.get_area());
    // The following does NOT work.
    // println!("b's side length (as Trait Obj): {}", b.get_side());

    // There's no way to go back down to a square.
    // The following (or anything like it) won't work.
    // let sq: Box<Square> = b;
    // println!("sq's side length: {}", sq.get_side());
}
