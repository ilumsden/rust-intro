#include <iostream>
#include <vector>

int fact_internal(const int val, int next_val, int inter, std::vector<int>& builders)
{
    if (next_val > val)
    {
        return inter;
    }
    builders.push_back(next_val);
    return fact_internal(val, next_val+1, inter*next_val, builders);
}

int fact(const int val, std::vector<int>& builders)
{
    if (val == 0)
    {
        return 1;
    }
    return fact_internal(val, 1, 1, builders);
}

int main()
{
    std::vector<int> vec;
    int val = 5;
    // 5! = 120
    val = fact(val, vec);
    std::cout << "5! = " << vec.at(0);
    for (unsigned int i = 1; i < vec.size(); i++)
    {
        std::cout << " * " << vec.at(i);
    }
    std::cout << " = " << val << std::endl;
}
